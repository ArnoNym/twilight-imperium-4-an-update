# Goals

- Rebalance factions
- No useless Techs/Abilities/...

# Components

- Integrate PoK in the Base Game as instructed
- No Mechs
- No Exploration
- No Relicfragments
- No Relics
- No Agents
- No Commanders
- No Heroes
- No Allicance Promissory Notes
- No Wormhole Nexus / Gamma Wormholes
- No legendary planets

## Techs
- Propulsion
    - Dark Energy Tap (Rework)
        - After you move ships into a system that does not contain any plantes you may gain one trade good.
        - Your ships can retreat into adjacent systems that do not contain other players' units, even if you do not have units or control planets in that system.
- Cybernetic
    - Scanlink Drone Network (Rework)
        - When you replenish commodities, gain two additional ones.
- Robotic
    - Self Assembly Routines (Rework)
        - After 1 or more of your units use PRODUCTION, you may exhaust this card to place 1 cruiser from your reinforcements in that system.

## Agendas
- Remove:
    - Articles of War (PoK)
    - Nexus Sovereignty (PoK)
    - Minister of Antiques (PoK)
    - Rearmament Agreement (PoK)
- Add, to make the total go back up to >=50:
    - Demilitarized Zone
    - Holy Planet of Ixth (adding a clause that makes the player lose the VP if they lose the card)
    - Shard of the Throne (adding a clause that makes the player lose the VP if they lose the card)
    - The Crown of Emphidia (adding a clause that makes the player lose the VP if they lose the card)
    - The Crown of Thalnos

## Objectives
- Remove
    - 1 Point
        - Discover Lost Outposts
        - Make History
    - 2 Points
        - Become A Legend
        - Reclaim Ancient Monuments
    - Secret
        - Betray A Friend (no alliance promissory node)
        - Darken The Skies (we don't want players to have to invade home systems since it punishes the weakest player)
        - Defy Space And Time (no legendary planets)
        - Destroy Heretical Works (no relics)
        - Mechanize The Military (no mechs)
        - Seize An Icon (no legendary planets)
        - Strenghten Bonds (no alliance promissory node)

## Action Cards
- Remove:
    - Archaeological Expedition (no exploration)
    - Exploration Probe (no exploration)
    - Seize Artifact (no artifacts)

# Base Factions

## Arborec

- Faction Abilities
    - Bioplasmosis (moved from Faction Tech)
        - "At the end of the status phase , you may remove any number of infantry from planets you control and place them on 1 or more planets you control in the same or adjacent systems."
- Faction Techs
    - Rapid Growth (moved from Commander)
        - "After another player activates a system that contains 1 or more of your units that have PRODUCTION: You may produce 1 unit in that system."
        - Prerequisites: g
- Flagship
    - Abilities
        - Production 5 (Rework)
- Promissory Note
    - “All your infantries in your active system gain Production 1 for this turn only. Then return this card to the Arborec player.” (Rework)

## Barony of Letnev

- Faction Abilities
    - "Munitions Reserve: At the start **of the first round** of space combat, you may spend **1** trade goods; you may re-roll any number of your dice during that combat round." (Rework)
- Flagship
    - Abilities
        - "'Direct Hit' cards are no longer effective against this type of ship." (New)

## Clan of Saar

- Flagship
    - Move
        - 2 (up from 1)

## Embers of Muaat

- Faction Abilities
    - "Iron Forge: After you spend a token from your strategy pool: You may gain 1 trade good." (moved from Commander)
- Flagship
    - Move
        - 2 (up from 1)

## Ermirates of Hacan

- Flagship
    - Abilities
        - "After you roll a die during a space combat in this system, you may spend 1 trade good to apply **+2** to the result." (Rework)

## Federation of Sol
- Promissory Note
    - "Remove 1 token from the Sol player's strategy pool **and return it to their reinforcements. Then,** you may place 2 infantry from your reinforcements on any planet you control." (Rework)
- Flagship
    - Abilities
        - "At the end of the status phase, place **2** infantry from your reinforcements in this system's space area **or on a planet you control in this system**." (Rework)

## Ghosts of Creuss

- Flagship
    - Move
        - 2 (up from 1)

## L1Z1X Mindnet

- Faction Abilities
    -  "Inheritance Systems: You may exhaust this card when you research a technology; ignore all of that technology's prerequisites." (Rework, Removed the 2 Ressources to spend)
- Flagship
    - Move
        - 2 (up from 1)

## Mentak Coalition

- Faction Abilities
    - "Kidnap: After you activate a system with a single enemy ship and you destroy this ship during space combat: You may force your opponent to give you 1 promissory note from their hand." (New)
- Flagship
    - Move
        - 2 (up from 1)

## Naalu Collective

## Nekro Virus

- Faction Abilities
    - "Superspread: After you gain a technology: You may draw 1 action card." (New)

## Sardakk N'orr

- Faction Abilities
    - "Swarm out: During the 'Commit Ground Forces' step: You can commit up to 1 ground force from each planet in the active system and each planet in adjacent systems that do not contain 1 of your command tokens. ” (New)
- Flagship
    - Combat
        - 5 (down from 6)
    - Ability also improves ground combat
- Starting Units
    - +2 Fighter

## Universities of Jol-Nar

- Promissory Note
    - "After the Jol-Nar player researches a technology that is not a faction technology: **Remove 1 token from the Jol-Nar player's strategy pool and return it to their reinforcements.  Then,** gain that technology."

## Winnu

- Faction Abilities
    - "Priorities: During combat: Apply +2 to the result of each of your unit's combat rolls in the Mecatol Rex system and your home system." (New)
- Promissory Note
    - "At the beginning of the Agenda Phase you may use this card like a refreshed 6 influence planet this Agenda Phase only. Then return this card to the Winnu player." (Rework)
- Faction Techs
    - "Lazax Gate Folding: During your tactical action treat Mecatol Rex’s system as if it contains both an alpha and beta wormhole." (Rework)
- Starting units
    - Infantry
        - 3 (up from 2)

## Xxcha Kingdom

- Faction Abilities
    - "Not to be silenced: Each planet you exhaust to cast votes provides 1 additional vote. Except the effect of your Promissory Note 'Political Secret'; game effects cannot prevent you from voting on an agenda." (moved from Commander)

## Yin Brotherhood

- Faction Technologies
    -  "Impulse Core: At the start of a space combat, you may destroy **up to 2** of your cruisers or destroyers in the active system to produce for each 1 hit against your opponent's ships; those hits must be assigned by your opponent to their non-fighter ships, if able." (Rework)
- Commodities
    - 3 (up from 2)

## Yssaril Tribes

- Flagship
    - Capacity
        - 5 (up from 3)

# PoK Factions

## The Argent Flight

## The Empyrean

## The Mahact Gene-Sorcerers

- Faction Abilities
    - "Imerpia: In another players turn you can remove this players command token from your fleet pool to let this player skip his turn, if he wants to." (Rework)
    - Remove Hubris

## The Naaz-Rokha Alliance

- Remove this faction from the game.

## The Nomad

- Faction Abilities
    - Remove The Company
- Faction Technologies
    - "Temporal Command Suite: After another player **resolves an action card**, you may exhaust this card to **return that action card to that players hand instead of discard pile**; you may perform a transaction with that player." (Rework)
- Flagship
    - Abilities
        - "You may treat this unit as if it were adjacent to systems that contain one or more of your **units**." (Rework)
- Upgraded Flagship
    - Cost
        - 0 (down from 8)

## The Titans of Ul

- Faction Abilities
    - "Terragenesis: After you gain control of a planet that does not have a sleeper token, you may place or move 1 sleeper token onto that planet." (Rework)

## The Vuil'Raith Cabal
